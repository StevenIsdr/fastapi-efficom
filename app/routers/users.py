# SYSTEM IMPORTS
import sys

sys.path.append("..")
# LIB IMPORTS
from fastapi import APIRouter, Response, status
# LOCAL IMPORTS
from app.models.user import User

router = APIRouter()


@router.get("/users")
async def get_all_users() -> list[User]:
    return users


@router.get("/users/{user_id}")
async def get_user_by_id(user_id: int):
    for u in users:
        if u.id == user_id:
            return u
    return {"error": "user not found"}


@router.post("/users")
async def create_user(user: User):
    if user.email in [u.email for u in users]:
        return Response(status_code=status.HTTP_409_CONFLICT)
    users.append(user)
    return user


@router.delete("/users/{user_id}")
async def delete_user(user_id: int):
    for i, u in enumerate(users):
        if u.id == user_id:
            users.pop(i)
            return {"message": "user deleted"}
    return {"error": "user not found"}


@router.put("/users/{user_id}")
async def update_user(user_id: int, user: User):
    for i, u in enumerate(users):
        if u.id == user_id:
            users[i] = user
            return {"message": "user updated"}
    return {"error": "user not found"}


users = [
    User(id=1, name="Leo", surname="Devo", email="leod1234444@gmail.com", password_hash="043a718774c572bd8a25adbeb1bfcd5c0256ae11cecf9f9c3f925d0e52beaf89", tel="s", newsletter=True,
         is_client=True),
    User(id=2, name="Seaux", surname="Sice", email="soo@si.ce", password_hash="s", tel="s", newsletter=True,
         is_client=True),
    User(id=3, name="John", surname="Doe", email="s@s", password_hash="s", tel="s", newsletter=True, is_client=True),
]
