# SYSTEM IMPORTS

# LIB IMPORTS
from fastapi import FastAPI

# LOCAL IMPORTS
from routers.users import router as users_router
from routers.users import users as users_list
from app.internal.auth import router as auth_router
from routers.cars import router as cars_router

app = FastAPI()

app.include_router(users_router, tags=["Users"])
app.include_router(cars_router, tags=["Cars"])
app.include_router(auth_router, tags=["Auth"])

users = users_list


# Create Users class lis

# Create CarType class list
