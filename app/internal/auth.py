import hashlib

from fastapi import Depends, HTTPException, APIRouter
from fastapi.security import OAuth2PasswordRequestForm

from app.routers.users import users

router = APIRouter()


@router.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    for u in users:
        if u.email == form_data.username:
            if u.password_hash == hash_password(form_data.password):
                return {"access_token": u.email, "token_type": "bearer"}
    raise HTTPException(status_code=401, detail="Incorrect email or password")


def hash_password(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()
