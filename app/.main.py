from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()



@app.get("/")
async def say_hello():
    """Ceci est une fonction qui dit bonjour oulala
    """
    return "bonjour"


class User(BaseModel):
    id: int
    name: str
    country: str | None = None


@app.get("/users", tags=["users"], response_model_exclude_unset=True)
async def get_all_users() -> list[User]:
    return users


@app.get("/users/search", tags=["users"])
async def search_users(name: str):
    return list(filter(lambda x: x["name"] == name, users))


@app.get("/users/{user_id}", tags=["users"])
async def get_user_by_id(user_id: int, name: str | None = None):
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None:
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    return filtred_list


@app.post("/users", tags=["users"])
async def create_user(user: User):
    users.append(user)
    return user


@app.put("/users/{user_id}", tags=["users"])
async def update_user(user_id: int, user: User):
    for i, u in enumerate(users):
        if u["id"] == user_id:
            users[i] = user
            return user
    return {"error": "user not found"}


@app.delete("/users/{user_id}", tags=["users"])
async def delete_user(user_id: int):
    for i, u in enumerate(users):
        if u["id"] == user_id:
            users.pop(i)
            return {"message": "user deleted"}
    return {"error": "user not found"}
