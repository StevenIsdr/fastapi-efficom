from enum import Enum
from pydantic import BaseModel


class CarType(BaseModel):
    id: int
    brand: str
    model: str
    doors_number: int
    engine: str


class CarState(str, Enum):
    NEW = "new"
    USED = "used"
    SOLD = "sold"


class Color(str, Enum):
    BLACK = "black"
    WHITE = "white"
    RED = "red"
    BLUE = "blue"
    GREEN = "green"
    YELLOW = "yellow"
    GREY = "grey"
    BROWN = "brown"
    ORANGE = "orange"
    PURPLE = "purple"
    PINK = "pink"
    SILVER = "silver"
    GOLD = "gold"
    OTHER = "other"


class Car(BaseModel):
    id: int
    km: int
    color: Color
    sell_price: int
    buy_price: int
    options: str
    state: CarState
    car_type: int
    construction_year: int
    parking_slot: int
    arrival_date: str
    delivery_date: str
    sale_employee: int | None = None
    previous_owner: int | None = None
    current_owner: int | None = None