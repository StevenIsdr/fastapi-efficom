# LIB IMPORTS
from pydantic import BaseModel

class User(BaseModel):
    id: int
    name: str
    surname: str
    email: str
    password_hash: str
    tel: str
    newsletter: bool
    is_client: bool