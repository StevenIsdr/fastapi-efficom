# SYSTEM IMPORTS
import sys
from typing import List

from fastapi.openapi.models import Response

sys.path.append("..")
# LIB IMPORTS
from fastapi import APIRouter, status
# LOCAL IMPORTS
from app.models.car import Car, CarType, Color, CarState
from app.routers.users import users

router = APIRouter()


@router.get("/cars")
async def get_all_cars() -> Response | list[Car]:
    if (len(cars)) == 0:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    return cars


@router.post("/cars", status_code=status.HTTP_201_CREATED)
async def create_car(car: Car):
    if car.sale_employee not in [u.id for u in users]:
        return {"error": "user not found"}
    if car.previous_owner not in [u.id for u in users]:
        return {"error": "user not found"}
    if car.current_owner not in [u.id for u in users]:
        return {"error": "user not found"}
    cars.append(car)
    return car


@router.delete("/cars/{car_id}")
async def delete_car(car_id: int):
    for i, u in enumerate(cars):
        if u.id == car_id:
            cars.pop(i)
            return {"message": "car deleted"}
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/cars/{car_id}")
async def update_car(car_id: int, car: Car):
    for i, u in enumerate(cars):
        if u.id == car_id:
            cars[i] = car
            return {"message": "car updated"}
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/cars/{car_id}")
async def get_car_by_id(car_id: int):
    for u in cars:
        if u.id == car_id:
            return u
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/cars/types")
async def get_all_car_types() -> Response | list[CarType]:
    if (len(car_types)) == 0:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    return car_types


@router.post("/cars/types", status_code=status.HTTP_201_CREATED)
async def create_car_type(car_type: CarType):
    car_types.append(car_type)
    return car_type


@router.delete("/cars/types/{car_type_id}")
async def delete_car_type(car_type_id: int):
    for i, u in enumerate(car_types):
        if u.id == car_type_id:
            car_types.pop(i)
            return {"message": "car type deleted"}
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/cars/types/{car_type_id}")
async def update_car_type(car_type_id: int, car_type: CarType):
    for i, u in enumerate(car_types):
        if u.id == car_type_id:
            car_types[i] = car_type
            return {"message": "car type updated"}
    return Response(status_code=status.HTTP_204_NO_CONTENT)


car_types = [
    CarType(id=1, brand="Renault", model="Clio", doors_number=5, engine="1.2L"),
    CarType(id=2, brand="BMW", model="Cliou", doors_number=5, engine="1.2L"),
    CarType(id=3, brand="Renault", model="Clito", doors_number=5, engine="1.2L"),
]

# Create Car class list
cars = [
    Car(id=1, km=10000, color=Color.BLACK, sell_price=10000, buy_price=10000, options="s", state=CarState.NEW,
        car_type=1, construction_year=2021, parking_slot=1, arrival_date="2021-01-01",
        delivery_date="2021-01-01", sale_employee=None, previous_owner=None, current_owner=None)]
